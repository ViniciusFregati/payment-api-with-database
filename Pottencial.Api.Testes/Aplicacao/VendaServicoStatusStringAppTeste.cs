﻿using AutoMapper;
using EnumsNET;
using Pottencial.Api.Aplicacao.AplicacaoServico;
using Pottencial.Api.Aplicacao.DTOs;
using Pottencial.Api.Aplicacao.Interfaces;
using Pottencial.Api.Aplicacao.Profiles;
using Pottencial.Api.Dominio.Interfaces.Repositorios;
using Pottencial.Api.Dominio.Interfaces.Servicos;
using Pottencial.Api.Dominio.Models;
using Pottencial.Api.Dominio.Servicos;
using Pottencial.Api.Infraestrutura.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Api.Testes.Aplicacao
{
    public class VendaServicoStatusStringAppTeste
    {
        private readonly IVendaServicoComStatusStringApp _servicoApp;
        private readonly VendaDTOSet _venda;
        public VendaServicoStatusStringAppTeste()
        {
            IVendaRepositorio repositorio = new VendaRepositorio();
            IVendaServico servico = new VendaServico(repositorio);

            MapperConfiguration configuracao = new(cfg =>
            {
                cfg.AddProfile(new VendaProfile());
                cfg.AddProfile(new VendedorProfile());
                cfg.AddProfile(new ProdutoProfile());
            });

            Mapper mapper = new(configuracao);

            IVendaServicoApp vendaServico = new VendaServicoApp(servico, mapper);
            _servicoApp = new VendaServicoComStatusStringApp(vendaServico);

            VendedorDTOSet vendedor = new()
            {
                Nome = "Vinícius Silva",
                Cpf = "485.256.125-75",
                Telefone = "(16)99421-5123",
                Email = "vinicius@gmail.com"
            };

            _venda = new VendaDTOSet
            {
                ItensVendidos = new List<ProdutoDTOSet>
                {
                    new ProdutoDTOSet()
                    {
                        NomeDoProduto = "Curso .NET Básico"
                    }
                },
                Vendedor = vendedor
            };

        }

        [Fact]
        public void TestaDescricaoDoEnumAguardandoPagamento()
        {
            //Arrange
            _servicoApp.RegistrarVenda(_venda);
            string enumEsperado = EnumStatusVenda.AguardandoPagamento.AsString(EnumFormat.Description);

            //Act
            string enumRecebido = _servicoApp.BuscarVendaPeloId(1).StatusVenda;

            //Assert
            Assert.Equal(enumEsperado, enumRecebido);
        }

        [Fact]
        public void TestaDescricaoDoEnumPagamentoAprovado()
        {
            //Arrange
            _servicoApp.RegistrarVenda(_venda);
            _servicoApp.AtualizarStatusVenda(1, EnumStatusVenda.PagamentoAprovado);
            string enumEsperado = EnumStatusVenda.PagamentoAprovado.AsString(EnumFormat.Description);

            //Act
            string enumRecebido = _servicoApp.BuscarVendaPeloId(1).StatusVenda;

            //Assert
            Assert.Equal(enumEsperado, enumRecebido);
        }

        [Fact]
        public void TestaDescricaoDoEnumEnviadoParaTransportadora()
        {
            //Arrange
            _servicoApp.RegistrarVenda(_venda);
            _servicoApp.AtualizarStatusVenda(1, EnumStatusVenda.PagamentoAprovado);
            _servicoApp.AtualizarStatusVenda(1, EnumStatusVenda.EnviadoParaTransportadora);
            string enumEsperado = EnumStatusVenda.EnviadoParaTransportadora.AsString(EnumFormat.Description);

            //Act
            string enumRecebido = _servicoApp.BuscarVendaPeloId(1).StatusVenda;

            //Assert
            Assert.Equal(enumEsperado, enumRecebido);
        }

        [Fact]
        public void TestaDescricaoDoEnumEntregue()
        {
            //Arrange
            _servicoApp.RegistrarVenda(_venda);
            _servicoApp.AtualizarStatusVenda(1, EnumStatusVenda.PagamentoAprovado);
            _servicoApp.AtualizarStatusVenda(1, EnumStatusVenda.EnviadoParaTransportadora);
            _servicoApp.AtualizarStatusVenda(1, EnumStatusVenda.Entregue);
            string enumEsperado = EnumStatusVenda.Entregue.AsString(EnumFormat.Description);

            //Act
            string enumRecebido = _servicoApp.BuscarVendaPeloId(1).StatusVenda;

            //Assert
            Assert.Equal(enumEsperado, enumRecebido);
        }

        [Fact]
        public void TestaDescricaoDoEnumCancelada()
        {
            //Arrange
            _servicoApp.RegistrarVenda(_venda);
            _servicoApp.AtualizarStatusVenda(1, EnumStatusVenda.Cancelada);
            string enumEsperado = EnumStatusVenda.Cancelada.AsString(EnumFormat.Description);

            //Act
            string enumRecebido = _servicoApp.BuscarVendaPeloId(1).StatusVenda;

            //Assert
            Assert.Equal(enumEsperado, enumRecebido);
        }
    }
}
