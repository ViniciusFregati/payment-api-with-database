﻿using AutoMapper;
using Pottencial.Api.Aplicacao.AplicacaoServico;
using Pottencial.Api.Aplicacao.DTOs;
using Pottencial.Api.Aplicacao.Interfaces;
using Pottencial.Api.Aplicacao.Profiles;
using Pottencial.Api.Dominio.Entidades;
using Pottencial.Api.Dominio.Excecoes;
using Pottencial.Api.Dominio.Interfaces.Servicos;
using Pottencial.Api.Dominio.Models;
using Pottencial.Api.Dominio.Servicos;
using Pottencial.Api.Infraestrutura.Interfaces;
using Pottencial.Api.Infraestrutura.Repositorio;

namespace Pottencial.Api.Testes.Aplicacao
{
    public class VendaServicoDBAppTeste
    {
        private readonly IVendaServicoApp _servicoApp;
        private readonly IControlaBancoDeDados _banco;
        private readonly VendaDTOSet _venda;
        public VendaServicoDBAppTeste()
        {
            VendaDBRepositorio repositorio = new();
            _banco = repositorio;
            _banco.IniciaTransacao();
            IVendaServico servico = new VendaServico(repositorio);

            MapperConfiguration configuracao = new(cfg =>
            {
                cfg.AddProfile(new VendaProfile());
                cfg.AddProfile(new VendedorProfile());
                cfg.AddProfile(new ProdutoProfile());
            });

            Mapper mapper = new(configuracao);

            _servicoApp = new VendaServicoApp(servico, mapper);

            VendedorDTOSet vendedor = new()
            {
                Nome = "TESTE Vinícius Silva",
                Cpf = "485.256.125-75",
                Telefone = "(16)99421-5123",
                Email = "vinicius@gmail.com"
            };

            _venda = new VendaDTOSet
            {
                ItensVendidos = new List<ProdutoDTOSet>
                {
                    new ProdutoDTOSet()
                    {
                        NomeDoProduto = "TESTE Curso .NET Básico"
                    }
                },
                Vendedor = vendedor
            };
        }

        [Fact]
        public void TestaRegistrarVenda()
        {
            //Arrange
            //Act
            Venda vendaRecebida = _servicoApp.RegistrarVenda(_venda);

            //Assert
            Assert.NotNull(vendaRecebida);
            Assert.NotNull(_servicoApp.BuscarVendaPeloId(vendaRecebida.Id));
        }

        [Fact]
        public void TestaBuscarVendaPeloId()
        {
            //Arrange
            int vendaCadastradaId = _servicoApp.RegistrarVenda(_venda).Id;

            //Act
            VendaDTOGet vendaRecebida = _servicoApp.BuscarVendaPeloId(vendaCadastradaId);

            //Assert
            Assert.NotNull(vendaRecebida);
            Assert.Equal(_venda.ItensVendidos[0].NomeDoProduto, vendaRecebida.ItensVendidos[0].NomeDoProduto);
        }

        [Theory]
        [InlineData(EnumStatusVenda.PagamentoAprovado)]
        [InlineData(EnumStatusVenda.Cancelada)]
        public void TestaAtualizarStatusVenda(EnumStatusVenda novoStatus)
        {
            //Arrange
            int vendaCadastradaId = _servicoApp.RegistrarVenda(_venda).Id;

            //Act
            bool resultado = _servicoApp.AtualizarStatusVenda(vendaCadastradaId, novoStatus);

            //Assert
            Assert.True(resultado);
        }

        [Theory]
        [InlineData(EnumStatusVenda.Entregue)]
        [InlineData(EnumStatusVenda.EnviadoParaTransportadora)]
        public void TestaNaoAtualizaStatusVenda(EnumStatusVenda novoStatus)
        {
            //Arrange
            int vendaCadastradaId = _servicoApp.RegistrarVenda(_venda).Id;

            //Act
            //Assert
            Assert.Throws<AtualizacaoStatusInvalidoException>(
                () => _servicoApp.AtualizarStatusVenda(vendaCadastradaId, novoStatus)
            );
        }

        public void Dispose()
        {
            _banco.DescartaTransacao();
        }
    }
}
