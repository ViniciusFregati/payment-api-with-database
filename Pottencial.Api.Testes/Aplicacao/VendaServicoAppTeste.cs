﻿using AutoMapper;
using Pottencial.Api.Aplicacao.AplicacaoServico;
using Pottencial.Api.Aplicacao.DTOs;
using Pottencial.Api.Aplicacao.Interfaces;
using Pottencial.Api.Aplicacao.Profiles;
using Pottencial.Api.Dominio.Entidades;
using Pottencial.Api.Dominio.Excecoes;
using Pottencial.Api.Dominio.Interfaces.Repositorios;
using Pottencial.Api.Dominio.Interfaces.Servicos;
using Pottencial.Api.Dominio.Models;
using Pottencial.Api.Dominio.Servicos;
using Pottencial.Api.Infraestrutura.Repositorio;

namespace Pottencial.Api.Testes.Aplicacao
{
    public class VendaServicoAppTeste
    {
        private readonly IVendaServicoApp _servicoApp;
        private readonly VendaDTOSet _venda;
        public VendaServicoAppTeste()
        {
            IVendaRepositorio repositorio = new VendaRepositorio();
            IVendaServico servico = new VendaServico(repositorio);

            MapperConfiguration configuracao = new(cfg =>
            {
                cfg.AddProfile(new VendaProfile());
                cfg.AddProfile(new VendedorProfile());
                cfg.AddProfile(new ProdutoProfile());
            });

            Mapper mapper = new(configuracao);

            _servicoApp = new VendaServicoApp(servico, mapper);

            VendedorDTOSet vendedor = new()
            {
                Nome = "Vinícius Silva",
                Cpf = "485.256.125-75",
                Telefone = "(16)99421-5123",
                Email = "vinicius@gmail.com"
            };

            _venda = new VendaDTOSet
            {
                ItensVendidos = new List<ProdutoDTOSet>
                {
                    new ProdutoDTOSet()
                    {
                        NomeDoProduto = "Curso .NET Básico"
                    }
                },
                Vendedor = vendedor
            };
        }

        [Fact]
        public void TestaRegistrarVenda()
        {
            //Arrange
            //Act
            Venda vendaRecebida = _servicoApp.RegistrarVenda(_venda);

            //Assert
            Assert.NotNull(vendaRecebida);
        }

        [Fact]
        public void TestaBuscarVendaPeloId()
        {
            //Arrange
            _servicoApp.RegistrarVenda(_venda);

            //Act
            VendaDTOGet vendaRecebida = _servicoApp.BuscarVendaPeloId(1);

            //Assert
            Assert.NotNull(vendaRecebida);
            Assert.Equal(_venda.ItensVendidos[0].NomeDoProduto, vendaRecebida.ItensVendidos[0].NomeDoProduto);
        }

        [Theory]
        [InlineData(EnumStatusVenda.PagamentoAprovado)]
        [InlineData(EnumStatusVenda.Cancelada)]
        public void TestaAtualizarStatusVenda(EnumStatusVenda novoStatus)
        {
            //Arrange
            _servicoApp.RegistrarVenda(_venda);

            //Act
            bool resultado = _servicoApp.AtualizarStatusVenda(1, novoStatus);

            //Assert
            Assert.True(resultado);
        }

        [Theory]
        [InlineData(EnumStatusVenda.Entregue)]
        [InlineData(EnumStatusVenda.EnviadoParaTransportadora)]
        public void TestaNaoAtualizaStatusVenda(EnumStatusVenda novoStatus)
        {
            //Arrange
            _servicoApp.RegistrarVenda(_venda);

            //Act
            //Assert
            Assert.Throws<AtualizacaoStatusInvalidoException>(
                () => _servicoApp.AtualizarStatusVenda(1, novoStatus)
            );
        }
    }
}
