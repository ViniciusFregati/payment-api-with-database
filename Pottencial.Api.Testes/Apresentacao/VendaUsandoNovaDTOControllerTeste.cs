﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Pottencial.Api.Aplicacao.AplicacaoServico;
using Pottencial.Api.Aplicacao.DTOs;
using Pottencial.Api.Aplicacao.Interfaces;
using Pottencial.Api.Aplicacao.Profiles;
using Pottencial.Api.Apresentacao.Controllers;
using Pottencial.Api.Dominio.Interfaces.Servicos;
using Pottencial.Api.Dominio.Servicos;
using Pottencial.Api.Infraestrutura.Interfaces;
using Pottencial.Api.Infraestrutura.Repositorio;

namespace Pottencial.Api.Testes.Apresentacao
{
    public class VendaUsandoNovaDTOControllerTeste
    {
        private readonly IVendaServicoApp _servicoApp;
        private readonly IControlaBancoDeDados _banco;
        private readonly VendaUsandoNovaDTOController _controller;
        private readonly VendaDTOSet _venda;

        public VendaUsandoNovaDTOControllerTeste()
        {
            VendaDBRepositorio repositorio = new();
            _banco = repositorio;
            _banco.IniciaTransacao();
            IVendaServico servico = new VendaServico(repositorio);

            MapperConfiguration configuracao = new(cfg =>
            {
                cfg.AddProfile(new VendaProfile());
                cfg.AddProfile(new VendedorProfile());
                cfg.AddProfile(new ProdutoProfile());
            });

            Mapper mapper = new(configuracao);

            _servicoApp = new VendaServicoApp(servico, mapper);
            IVendaServicoComStatusStringApp vendaServico = new VendaServicoComStatusStringApp(_servicoApp);
            VendaController vendaController = new(_servicoApp, mapper);
            _controller = new VendaUsandoNovaDTOController(vendaServico, vendaController);

            VendedorDTOSet vendedor = new()
            {
                Nome = "TESTE Vinícius Silva",
                Cpf = "485.256.125-75",
                Telefone = "(16)99421-5123",
                Email = "vinicius@gmail.com"
            };

            _venda = new VendaDTOSet
            {
                ItensVendidos = new List<ProdutoDTOSet>
                {
                    new ProdutoDTOSet()
                    {
                        NomeDoProduto = "TESTE Curso .NET Básico"
                    }
                },
                Vendedor = vendedor
            };
        }

        [Theory]
        [InlineData(StatusCodes.Status200OK, true)]
        [InlineData(StatusCodes.Status404NotFound, false)]
        public void TestaObterVendaPorIdComIDValido(int statusEsperado, bool vendaExiste)
        {
            //Arrange
            int id = vendaExiste ? _servicoApp.RegistrarVenda(_venda).Id : 0;

            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult) _controller.ObterVendaPorId(id);

            //Assert
            Assert.Equal(statusEsperado, resultado.StatusCode);
        }

        public void Dispose()
        {
            _banco.DescartaTransacao();
        }
    }
}