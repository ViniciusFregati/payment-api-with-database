﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Pottencial.Api.Aplicacao.AplicacaoServico;
using Pottencial.Api.Aplicacao.DTOs;
using Pottencial.Api.Aplicacao.Interfaces;
using Pottencial.Api.Aplicacao.Profiles;
using Pottencial.Api.Apresentacao.Controllers;
using Pottencial.Api.Dominio.Interfaces.Repositorios;
using Pottencial.Api.Dominio.Interfaces.Servicos;
using Pottencial.Api.Dominio.Models;
using Pottencial.Api.Dominio.Servicos;
using Pottencial.Api.Infraestrutura.Repositorio;

namespace Pottencial.Api.Testes.Apresentacao
{
    public class VendaControllerTeste
    {
        private readonly VendaController _controller;
        private readonly VendaDTOSet _venda;

        public VendaControllerTeste()
        {
            IVendaRepositorio repositorio = new VendaRepositorio();
            IVendaServico servico = new VendaServico(repositorio);

            MapperConfiguration configuracao = new(cfg =>
            {
                cfg.AddProfile(new VendaProfile());
                cfg.AddProfile(new VendedorProfile());
                cfg.AddProfile(new ProdutoProfile());
            });

            Mapper mapper = new(configuracao);

            IVendaServicoApp servicoApp = new VendaServicoApp(servico, mapper);
            _controller = new VendaController(servicoApp, mapper);

            VendedorDTOSet vendedor = new()
            {
                Nome = "Vinícius Silva",
                Cpf = "485.256.125-75",
                Telefone = "(16)99421-5123",
                Email = "vinicius@gmail.com"
            };

            _venda = new VendaDTOSet
            {
                ItensVendidos = new List<ProdutoDTOSet>
                {
                    new ProdutoDTOSet()
                    {
                        NomeDoProduto = "Curso .NET Básico"
                    }
                },
                Vendedor = vendedor
            };
        }

        [Fact]
        public void TestaCadastrarVendaComValoresValidos()
        {
            //Arrange
            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult)_controller.CadastrarVenda(_venda);

            //Assert
            Assert.Equal(StatusCodes.Status201Created, resultado.StatusCode);
        }

        [Theory]
        [InlineData(false, "(17)99124-6012", "485.152.741-90")]
        [InlineData(true, "1799124-6012", "485.152.741-90")]
        [InlineData(true, "(17)99124-6012", "485152.741-90")]
        public void TestaCadastrarVendaComDadoInvalido(bool possuiItens, string telefone, string cpf)
        {
            //Arrange
            List<ProdutoDTOSet> itens = new();
            if (possuiItens)
                itens.Add(new ProdutoDTOSet() { NomeDoProduto = "Curso .NET Básico" });

            VendaDTOSet venda = new()
            {
                ItensVendidos = itens,
                Vendedor = new VendedorDTOSet
                {
                    Telefone = telefone,
                    Nome = "André Silva",
                    Email = "andre@gmail.com",
                    Cpf = cpf
                }
            };

            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult)_controller.CadastrarVenda(venda);

            //Assert
            Assert.Equal(StatusCodes.Status400BadRequest, resultado.StatusCode);
        }

        [Fact]
        public void TestaObterVendaPorIdComIdValido()
        {
            //Arrange
            _controller.CadastrarVenda(_venda);

            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult)_controller.ObterVendaPorId(1);

            //Assert
            Assert.Equal(StatusCodes.Status200OK, resultado.StatusCode);
        }

        [Fact]
        public void TestaObterVendaPorIdComIdNaoCadastrado()
        {
            //Arrange
            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult)_controller.ObterVendaPorId(1);

            //Assert
            Assert.Equal(StatusCodes.Status404NotFound, resultado.StatusCode);
        }

        [Fact]
        public void TestaAtualizarStatusVenda()
        {
            //Arrange
            _controller.CadastrarVenda(_venda);

            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult)_controller.
                                                 AtualizarStatusVenda(1, EnumStatusVenda.PagamentoAprovado);

            //Assert
            Assert.Equal(StatusCodes.Status204NoContent, resultado.StatusCode);
        }

        [Theory]
        [InlineData(EnumStatusVenda.Entregue)]
        [InlineData(EnumStatusVenda.EnviadoParaTransportadora)]
        public void TestaErroAtualizarStatusVenda(EnumStatusVenda novoStatus)
        {
            //Arrange
            _controller.CadastrarVenda(_venda);

            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult)_controller.
                                                 AtualizarStatusVenda(1, novoStatus);
            //Assert
            Assert.Equal(StatusCodes.Status400BadRequest, resultado.StatusCode);
        }

        [Fact]
        public void TestaAtualizarStatusVendaComIdInvalido()
        {
            //Arrange
            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult)_controller.
                                                 AtualizarStatusVenda(1, EnumStatusVenda.PagamentoAprovado);
            //Assert
            Assert.Equal(StatusCodes.Status404NotFound, resultado.StatusCode);
        }
    }
}
