﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Pottencial.Api.Aplicacao.AplicacaoServico;
using Pottencial.Api.Aplicacao.DTOs;
using Pottencial.Api.Aplicacao.Interfaces;
using Pottencial.Api.Aplicacao.Profiles;
using Pottencial.Api.Apresentacao.Controllers;
using Pottencial.Api.Dominio.Interfaces.Servicos;
using Pottencial.Api.Dominio.Models;
using Pottencial.Api.Dominio.Servicos;
using Pottencial.Api.Infraestrutura.Interfaces;
using Pottencial.Api.Infraestrutura.Repositorio;

namespace Pottencial.Api.Testes.Apresentacao
{
    public class VendaControllerDBTeste
    {
        private readonly IVendaServicoApp _servicoApp;
        private readonly IControlaBancoDeDados _banco;
        private readonly VendaController _controller;
        private readonly VendaDTOSet _venda;

        public VendaControllerDBTeste()
        {
            VendaDBRepositorio repositorio = new();
            _banco = repositorio;
            _banco.IniciaTransacao();
            IVendaServico servico = new VendaServico(repositorio);

            MapperConfiguration configuracao = new(cfg =>
            {
                cfg.AddProfile(new VendaProfile());
                cfg.AddProfile(new VendedorProfile());
                cfg.AddProfile(new ProdutoProfile());
            });

            Mapper mapper = new(configuracao);

            _servicoApp = new VendaServicoApp(servico, mapper);
            _controller = new VendaController(_servicoApp, mapper);

            VendedorDTOSet vendedor = new()
            {
                Nome = "TESTE Vinícius Silva",
                Cpf = "485.256.125-75",
                Telefone = "(16)99421-5123",
                Email = "vinicius@gmail.com"
            };

            _venda = new VendaDTOSet
            {
                ItensVendidos = new List<ProdutoDTOSet>
                {
                    new ProdutoDTOSet()
                    {
                        NomeDoProduto = "TESTE Curso .NET Básico"
                    }
                },
                Vendedor = vendedor
            };
        }

        [Fact]
        public void TestaCadastrarVendaComValoresValidos()
        {
            //Arrange
            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult)_controller.CadastrarVenda(_venda);

            //Assert
            Assert.Equal(StatusCodes.Status201Created, resultado.StatusCode);
        }

        [Theory]
        [InlineData(false, "(17)99124-6012", "485.152.741-90")]
        [InlineData(true, "1799124-6012", "485.152.741-90")]
        [InlineData(true, "(17)99124-6012", "485152.741-90")]
        public void TestaCadastrarVendaComDadoInvalido(bool possuiItens, string telefone, string cpf)
        {
            //Arrange
            List<ProdutoDTOSet> itens = new();
            if (possuiItens)
                itens.Add(new ProdutoDTOSet() { NomeDoProduto = "TESTE Curso .NET Básico" });

            VendaDTOSet venda = new()
            {
                ItensVendidos = itens,
                Vendedor = new VendedorDTOSet
                {
                    Telefone = telefone,
                    Nome = "TESTE André Silva",
                    Email = "andre@gmail.com",
                    Cpf = cpf
                }
            };

            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult)_controller.CadastrarVenda(venda);

            //Assert
            Assert.Equal(StatusCodes.Status400BadRequest, resultado.StatusCode);
        }

        [Fact]
        public void TestaObterVendaPorIdComIdValido()
        {
            //Arrange
            int vendaCadastradaId = _servicoApp.RegistrarVenda(_venda).Id;

            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult)_controller.ObterVendaPorId(vendaCadastradaId);

            //Assert
            Assert.Equal(StatusCodes.Status200OK, resultado.StatusCode);
        }

        [Fact]
        public void TestaObterVendaPorIdComIdNaoCadastrado()
        {
            //Arrange
            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult)_controller.ObterVendaPorId(0);

            //Assert
            Assert.Equal(StatusCodes.Status404NotFound, resultado.StatusCode);
        }

        [Fact]
        public void TestaAtualizarStatusVenda()
        {
            //Arrange
            int vendaCriadaId = _servicoApp.RegistrarVenda(_venda).Id;

            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult)_controller.
                                                 AtualizarStatusVenda(vendaCriadaId, EnumStatusVenda.PagamentoAprovado);

            //Assert
            Assert.Equal(StatusCodes.Status204NoContent, resultado.StatusCode);
        }

        [Theory]
        [InlineData(EnumStatusVenda.Entregue)]
        [InlineData(EnumStatusVenda.EnviadoParaTransportadora)]
        public void TestaErroAtualizarStatusVenda(EnumStatusVenda novoStatus)
        {
            //Arrange
            int vendaCriadaId = _servicoApp.RegistrarVenda(_venda).Id;

            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult)_controller.
                                                 AtualizarStatusVenda(vendaCriadaId, novoStatus);
            //Assert
            Assert.Equal(StatusCodes.Status400BadRequest, resultado.StatusCode);
        }

        [Fact]
        public void TestaAtualizarStatusVendaComIdInvalido()
        {
            //Arrange
            //Act
            IStatusCodeActionResult resultado = (IStatusCodeActionResult)_controller.
                                                 AtualizarStatusVenda(0, EnumStatusVenda.PagamentoAprovado);
            //Assert
            Assert.Equal(StatusCodes.Status404NotFound, resultado.StatusCode);
        }

        public void Dispose()
        {
            _banco.DescartaTransacao();
        }
    }
}
