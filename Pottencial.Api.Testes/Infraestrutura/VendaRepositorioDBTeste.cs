﻿using Pottencial.Api.Dominio.Entidades;
using Pottencial.Api.Dominio.Excecoes;
using Pottencial.Api.Dominio.Interfaces.Repositorios;
using Pottencial.Api.Dominio.Models;
using Pottencial.Api.Infraestrutura.Interfaces;
using Pottencial.Api.Infraestrutura.Repositorio;

namespace Pottencial.Api.Testes.Infraestrutura
{
    public class VendaRepositorioDBTeste
    {
        private readonly IVendaRepositorio _repositorio;
        private readonly IControlaBancoDeDados _banco;
        private readonly Venda _venda;

        public VendaRepositorioDBTeste()
        {
            VendaDBRepositorio  repositorio = new();
            _banco = repositorio;
            _banco.IniciaTransacao();
            _repositorio = repositorio;

            _venda = new Venda
            {
                Vendedor = new Vendedor
                {
                    Nome = "TESTE Vinícius",
                    Email = "vinicius@gmail.com",
                    Cpf = "487.412.723-86",
                    Telefone = "(16)98512-1234"
                },
                ItensVendidos = new List<Produto>
                {
                    new Produto()
                    {
                        NomeDoProduto = "TESTE Piano"
                    }
                }
            };
        }

        [Fact]
        public void TestaRegistrarVenda()
        {
            //Arrange
            //Act
            Venda vendaRecebida = _repositorio.RegistrarVenda(_venda);

            //Assert
            Assert.NotNull(vendaRecebida);
            Assert.Equal(EnumStatusVenda.AguardandoPagamento, vendaRecebida.StatusVenda);
        }

        [Fact]
        public void TestaObterVendaPorId()
        {
            //Arrange
            Venda vendaRecebida = _repositorio.RegistrarVenda(_venda);

            //Act
            Venda vendaProcurada = _repositorio.BuscarVendaPeloId(vendaRecebida.Id);

            //Assert
            Assert.NotNull(vendaProcurada);
            Assert.Equal(_venda.ItensVendidos[0], vendaProcurada.ItensVendidos[0]);
        }

        [Theory]
        [InlineData(EnumStatusVenda.PagamentoAprovado)]
        [InlineData(EnumStatusVenda.Cancelada)]
        public void TestaAtualizarStatusDaVendaComStatusInicialAguardandoPagamento(EnumStatusVenda statusVenda)
        {
            //Arrange
            Venda vendaCriada = _repositorio.RegistrarVenda(_venda);

            //Act
            bool resposta = _repositorio.AtualizarStatusVenda(vendaCriada.Id, statusVenda);

            //Assert
            Assert.True(resposta);
            Assert.Equal(statusVenda, _repositorio.BuscarVendaPeloId(vendaCriada.Id).StatusVenda);
        }

        [Theory]
        [InlineData(EnumStatusVenda.EnviadoParaTransportadora)]
        [InlineData(EnumStatusVenda.Cancelada)]
        public void TestaAtualizarStatusDaVendaComStatusInicialPagamentoAprovado(EnumStatusVenda statusVenda)
        {
            //Arrange
            int vendaCriadaId = _repositorio.RegistrarVenda(_venda).Id;
            _repositorio.AtualizarStatusVenda(vendaCriadaId, EnumStatusVenda.PagamentoAprovado);

            //Act
            bool resposta = _repositorio.AtualizarStatusVenda(vendaCriadaId, statusVenda);

            //Assert
            Assert.True(resposta);
            Assert.Equal(statusVenda, _repositorio.BuscarVendaPeloId(vendaCriadaId).StatusVenda);
        }

        [Fact]
        public void TestaAtualizarStatusVendaComStatusInicialEnviadoParaTransportadora()
        {
            //Arrange
            int vendaCriadaId = _repositorio.RegistrarVenda(_venda).Id;
            _repositorio.AtualizarStatusVenda(vendaCriadaId, EnumStatusVenda.PagamentoAprovado);
            _repositorio.AtualizarStatusVenda(vendaCriadaId, EnumStatusVenda.EnviadoParaTransportadora);

            //Act
            bool resposta = _repositorio.AtualizarStatusVenda(vendaCriadaId, EnumStatusVenda.Entregue);

            //Assert
            Assert.True(resposta);
            Assert.Equal(EnumStatusVenda.Entregue, _repositorio.BuscarVendaPeloId(vendaCriadaId).StatusVenda);
        }

        [Theory]
        [InlineData(EnumStatusVenda.EnviadoParaTransportadora)]
        [InlineData(EnumStatusVenda.Entregue)]
        public void TestaNaoAtualizarStatusVendaIniciandoComoAguardandoPagamento(EnumStatusVenda statusVenda)
        {
            //Arrange
            int vendaCriadaId = _repositorio.RegistrarVenda(_venda).Id;

            //Act
            //Assert
            Assert.Throws<AtualizacaoStatusInvalidoException>(
                () => _repositorio.AtualizarStatusVenda(vendaCriadaId, statusVenda)
            );
            Assert.Equal(EnumStatusVenda.AguardandoPagamento, _repositorio.BuscarVendaPeloId(vendaCriadaId).StatusVenda);
        }

        [Theory]
        [InlineData(EnumStatusVenda.AguardandoPagamento)]
        [InlineData(EnumStatusVenda.Entregue)]
        public void TestaNaoAtualizarStatusVendaIniciandoComoPagamentoAprovado(EnumStatusVenda statusVenda)
        {
            //Arrange
            int vendaCriadaId = _repositorio.RegistrarVenda(_venda).Id;
            _repositorio.AtualizarStatusVenda(vendaCriadaId, EnumStatusVenda.PagamentoAprovado);

            //Act
            //Assert
            Assert.Throws<AtualizacaoStatusInvalidoException>(
                () => _repositorio.AtualizarStatusVenda(vendaCriadaId, statusVenda)
            );
            Assert.Equal(EnumStatusVenda.PagamentoAprovado, _repositorio.BuscarVendaPeloId(vendaCriadaId).StatusVenda);
        }

        [Theory]
        [InlineData(EnumStatusVenda.AguardandoPagamento)]
        [InlineData(EnumStatusVenda.PagamentoAprovado)]
        [InlineData(EnumStatusVenda.Cancelada)]
        public void TestaNaoAtualizarStatusVendaIniciandoComoEnviadoParaTransportadora(EnumStatusVenda statusVenda)
        {
            //Arrange
            int vendaCriadaId = _repositorio.RegistrarVenda(_venda).Id;
            _repositorio.AtualizarStatusVenda(vendaCriadaId, EnumStatusVenda.PagamentoAprovado);
            _repositorio.AtualizarStatusVenda(vendaCriadaId, EnumStatusVenda.EnviadoParaTransportadora);

            //Act
            //Assert
            Assert.Throws<AtualizacaoStatusInvalidoException>(
                () => _repositorio.AtualizarStatusVenda(vendaCriadaId, statusVenda)
            );
            Assert.Equal(EnumStatusVenda.EnviadoParaTransportadora, _repositorio.BuscarVendaPeloId(vendaCriadaId).StatusVenda);
        }

        public void Dispose()
        {
            _banco.DescartaTransacao();
        }
    }
}