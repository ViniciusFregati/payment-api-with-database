﻿using Pottencial.Api.Dominio.Entidades;
using Pottencial.Api.Dominio.Excecoes;
using Pottencial.Api.Dominio.Interfaces.Repositorios;
using Pottencial.Api.Dominio.Models;
using Pottencial.Api.Infraestrutura.Repositorio;

namespace Pottencial.Api.Testes.Infraestrutura
{
    public class VendaRepositorioTeste
    {
        private readonly IVendaRepositorio _repositorio;
        private readonly Venda _venda;

        public VendaRepositorioTeste()
        {
            _repositorio = new VendaRepositorio();

            _venda = new Venda
            {
                Vendedor = new Vendedor
                {
                    Nome = "Vinícius",
                    Email = "vinicius@gmail.com",
                    Cpf = "487.412.723-86",
                    Telefone = "(16)98512-1234"
                },
                ItensVendidos = new List<Produto>
                {
                    new Produto()
                    {
                        NomeDoProduto = "Piano"
                    }
                }
            };
        }

        [Fact]
        public void TestaRegistrarVenda()
        {
            //Arrange
            //Act
            Venda vendaRecebida = _repositorio.RegistrarVenda(_venda);

            //Assert
            Assert.NotNull(vendaRecebida);
        }

        [Fact]
        public void TestaObterVendaPorId()
        {
            //Arrange
            _repositorio.RegistrarVenda(_venda);

            //Act
            Venda vendaRecebida = _repositorio.BuscarVendaPeloId(1);

            //Assert
            Assert.NotNull(vendaRecebida);
            Assert.Equal(_venda.ItensVendidos[0], vendaRecebida.ItensVendidos[0]);
        }

        [Theory]
        [InlineData(EnumStatusVenda.PagamentoAprovado)]
        [InlineData(EnumStatusVenda.Cancelada)]
        public void TestaAtualizarStatusDaVendaComStatusInicialAguardandoPagamento(EnumStatusVenda statusVenda)
        {
            //Arrange
            _repositorio.RegistrarVenda(_venda);

            //Act
            bool resposta = _repositorio.AtualizarStatusVenda(1, statusVenda);

            //Assert
            Assert.True(resposta);
            Assert.Equal(statusVenda, _repositorio.BuscarVendaPeloId(1).StatusVenda);
        }

        [Theory]
        [InlineData(EnumStatusVenda.EnviadoParaTransportadora)]
        [InlineData(EnumStatusVenda.Cancelada)]
        public void TestaAtualizarStatusDaVendaComStatusInicialPagamentoAprovado(EnumStatusVenda statusVenda)
        {
            //Arrange
            _repositorio.RegistrarVenda(_venda);
            _repositorio.AtualizarStatusVenda(1, EnumStatusVenda.PagamentoAprovado);

            //Act
            bool resposta = _repositorio.AtualizarStatusVenda(1, statusVenda);

            //Assert
            Assert.True(resposta);
            Assert.Equal(statusVenda, _repositorio.BuscarVendaPeloId(1).StatusVenda);
        }

        [Fact]
        public void TestaAtualizarStatusVendaComStatusInicialEnviadoParaTransportadora()
        {
            //Arrange
            _repositorio.RegistrarVenda(_venda);
            _repositorio.AtualizarStatusVenda(1, EnumStatusVenda.PagamentoAprovado);
            _repositorio.AtualizarStatusVenda(1, EnumStatusVenda.EnviadoParaTransportadora);

            //Act
            bool resposta = _repositorio.AtualizarStatusVenda(1, EnumStatusVenda.Entregue);

            //Assert
            Assert.True(resposta);
            Assert.Equal(EnumStatusVenda.Entregue, _repositorio.BuscarVendaPeloId(1).StatusVenda);
        }

        [Theory]
        [InlineData(EnumStatusVenda.EnviadoParaTransportadora)]
        [InlineData(EnumStatusVenda.Entregue)]
        public void TestaNaoAtualizarStatusVendaIniciandoComoAguardandoPagamento(EnumStatusVenda statusVenda)
        {
            //Arrange
            _repositorio.RegistrarVenda(_venda);

            //Act
            //Assert
            Assert.Throws<AtualizacaoStatusInvalidoException>(
                () => _repositorio.AtualizarStatusVenda(1, statusVenda)
            );
            Assert.Equal(EnumStatusVenda.AguardandoPagamento, _repositorio.BuscarVendaPeloId(1).StatusVenda);
        }

        [Theory]
        [InlineData(EnumStatusVenda.AguardandoPagamento)]
        [InlineData(EnumStatusVenda.Entregue)]
        public void TestaNaoAtualizarStatusVendaIniciandoComoPagamentoAprovado(EnumStatusVenda statusVenda)
        {
            //Arrange
            _repositorio.RegistrarVenda(_venda);
            _repositorio.AtualizarStatusVenda(1, EnumStatusVenda.PagamentoAprovado);

            //Act
            //Assert
            Assert.Throws<AtualizacaoStatusInvalidoException>(
                () => _repositorio.AtualizarStatusVenda(1, statusVenda)
            );
            Assert.Equal(EnumStatusVenda.PagamentoAprovado, _repositorio.BuscarVendaPeloId(1).StatusVenda);
        }

        [Theory]
        [InlineData(EnumStatusVenda.AguardandoPagamento)]
        [InlineData(EnumStatusVenda.PagamentoAprovado)]
        [InlineData(EnumStatusVenda.Cancelada)]
        public void TestaNaoAtualizarStatusVendaIniciandoComoEnviadoParaTransportadora(EnumStatusVenda statusVenda)
        {
            //Arrange
            _repositorio.RegistrarVenda(_venda);
            _repositorio.AtualizarStatusVenda(1, EnumStatusVenda.PagamentoAprovado);
            _repositorio.AtualizarStatusVenda(1, EnumStatusVenda.EnviadoParaTransportadora);

            //Act
            //Assert
            Assert.Throws<AtualizacaoStatusInvalidoException>(
                () => _repositorio.AtualizarStatusVenda(1, statusVenda)
            );
            Assert.Equal(EnumStatusVenda.EnviadoParaTransportadora, _repositorio.BuscarVendaPeloId(1).StatusVenda);
        }
    }
}
