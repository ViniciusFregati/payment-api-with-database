﻿using Pottencial.Api.Infraestrutura.Interfaces;
using Pottencial.Api.Infraestrutura.Repositorio;

namespace Pottencial.Api.Testes.Infraestrutura
{
    public class TesteConexaoComDB
    {
        [Fact]
        public void TestaConexao()
        {
            //Arrange
            IControlaBancoDeDados repositorio = new VendaDBRepositorio();
            bool conectado;

            //Act
            conectado = repositorio.ConsegueSeConectarComBanco();

            //Assert
            Assert.True(conectado);
        }
    }
}
