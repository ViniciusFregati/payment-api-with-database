﻿using Pottencial.Api.Dominio.Entidades;
using Pottencial.Api.Dominio.Models;

namespace Pottencial.Api.Dominio.Interfaces.Repositorios
{
    public interface IVendaRepositorio
    {
        public Venda RegistrarVenda(Venda venda);
        public Venda BuscarVendaPeloId(int id);
        public bool AtualizarStatusVenda(int id, EnumStatusVenda novoStatus);
    }
}
