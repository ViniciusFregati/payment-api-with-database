﻿namespace Pottencial.Api.Dominio.Models
{
    /// <summary>
    /// Classe de apoio a qual possuí todas as possíveis atualizações de status de uma venda.
    /// </summary>
    public class AtualizacaoDeStatusDeVenda
    {
        public readonly List<EnumStatusVenda> InicioAguardandoPagamento = new() { EnumStatusVenda.PagamentoAprovado, EnumStatusVenda.Cancelada };
        public readonly List<EnumStatusVenda> InicioPagamentoAprovado = new() { EnumStatusVenda.EnviadoParaTransportadora, EnumStatusVenda.Cancelada };
        public readonly List<EnumStatusVenda> InicioEnviadoParaTransportadora = new() { EnumStatusVenda.Entregue };
    }
}
