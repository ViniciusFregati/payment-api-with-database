﻿using System.ComponentModel;

namespace Pottencial.Api.Dominio.Models
{
    /// <summary>
    /// Enum com todos os status de venda.
    /// </summary>
    public enum EnumStatusVenda
    {
        [DescriptionAttribute("Aguardando pagamento")]
        AguardandoPagamento = 0,
        [DescriptionAttribute("Pagamento aprovado")]
        PagamentoAprovado = 1,
        [DescriptionAttribute("Enviado para transportadora")]
        EnviadoParaTransportadora = 2,
        [DescriptionAttribute("Entregue")]
        Entregue = 3,
        [DescriptionAttribute("Cancelada")]
        Cancelada = 4,
    }
}
