﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace Pottencial.Api.Dominio.Excecoes
{
    [Serializable]
    public class SemConexaoComOBancoException : Exception
    {
        public SemConexaoComOBancoException() { }
        public SemConexaoComOBancoException(string message) : base(message) { }
        public SemConexaoComOBancoException(string message, Exception innerException) : base(message, innerException) { }
    }
}
