﻿namespace Pottencial.Api.Dominio.Excecoes
{
    [Serializable]
    public class NaoEncontradoException : Exception
    {
        public NaoEncontradoException() { }
        public NaoEncontradoException(string message) : base(message) { }
        public NaoEncontradoException(string message, Exception innerException) : base(message, innerException) { }
    }
}
