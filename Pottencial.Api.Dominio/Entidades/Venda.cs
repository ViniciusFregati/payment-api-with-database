﻿using Pottencial.Api.Dominio.Models;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Api.Dominio.Entidades
{
    /// <summary>
    /// Entidade que ilustra uma venda, contém as principais informações referentes a ela.
    /// </summary>
    public class Venda
    {
        [Key]
        public int Id { get; set; }
        public int VendedorId { get; set; }
        [Required(ErrorMessage = "O campo vendedor é obrigatório")]
        public virtual Vendedor? Vendedor { get; set; }
        [Required(ErrorMessage = "O campo data é obrigatório")]
        public DateTime Data { get; set; }
        [Required(ErrorMessage = "O campo itens vendidos é obrigatório")]
        public virtual List<Produto>? ItensVendidos { get; set; }
        [Required(ErrorMessage = "O campo status venda é obrigatório")]
        public EnumStatusVenda StatusVenda { get; set; }
    }
}
