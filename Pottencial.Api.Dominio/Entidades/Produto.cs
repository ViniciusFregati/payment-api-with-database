﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Pottencial.Api.Dominio.Entidades
{
    public class Produto
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "O campo nome do produto é obrigatório")]
        public string? NomeDoProduto { get; set; }
        [Required(ErrorMessage = "O campo id da venda é obrigatório")]
        public int VendaId { get; set; }
    }
}
