﻿using AutoMapper;
using Pottencial.Api.Aplicacao.DTOs;
using Pottencial.Api.Dominio.Entidades;

namespace Pottencial.Api.Aplicacao.Profiles
{
    public class ProdutoProfile : Profile
    {
        public ProdutoProfile()
        {
            CreateMap<Produto, ProdutoDTOGet>();
            CreateMap<ProdutoDTOSet, Produto>();
        }
    }
}
