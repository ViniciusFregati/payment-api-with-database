﻿using AutoMapper;
using Pottencial.Api.Aplicacao.DTOs;
using Pottencial.Api.Aplicacao.Interfaces;
using Pottencial.Api.Dominio.Entidades;
using Pottencial.Api.Dominio.Excecoes;
using Pottencial.Api.Dominio.Interfaces.Servicos;
using Pottencial.Api.Dominio.Models;

namespace Pottencial.Api.Aplicacao.AplicacaoServico
{
    /// <summary>
    /// Realiza a integração dos métodos existentes e dos DTOs, melhorando 
    /// a criação e visualização de seus objetos
    /// </summary>
    public class VendaServicoApp : IVendaServicoApp
    {
        private readonly IVendaServico _servico;
        private readonly IMapper _mapper;

        public VendaServicoApp(IVendaServico servico, IMapper mapper)
        {
            _servico = servico;
            _mapper = mapper;
        }

        /// <summary>
        /// Atualiza o status de uma venda.
        /// </summary>
        /// <param name="id">Id da venda que se deseja atualizar.</param>
        /// <param name="novoStatus">Status que se deseja atribuir a venda.</param>
        /// <returns>Retorna um booleano dizendo se deu certo a atualização</returns>
        /// <exception cref="AtualizacaoStatusInvalidoException"></exception>
        /// <exception cref="NaoEncontradoException"></exception>
        /// <exception cref="SemConexaoComOBancoException"></exception>
        public bool AtualizarStatusVenda(int id, EnumStatusVenda novoStatus)
        {
            return _servico.AtualizarStatusVenda(id, novoStatus);
        }

        /// <summary>
        /// Busca uma venda pelo seu Id.
        /// </summary>
        /// <param name="id">Id da venda que se deseja buscar.</param>
        /// <returns>Retorna o DTO da venda encontrada.</returns>
        /// <exception cref="NaoEncontradoException"></exception>
        /// <exception cref="SemConexaoComOBancoException"></exception>
        public VendaDTOGet BuscarVendaPeloId(int id)
        {
            Venda vendaBuscada = _servico.BuscarVendaPeloId(id);
            VendaDTOGet vendaConvertida = _mapper.Map<VendaDTOGet>(vendaBuscada);

            return vendaConvertida;
        }

        /// <summary>
        /// Registra uma venda.
        /// </summary>
        /// <param name="venda">DTO da venda que se deseja registrar.</param>
        /// <returns>Retorna a venda registrada.</returns>
        /// <exception cref="ListaVaziaException"></exception>
        /// <exception cref="CpfInvalidoException"></exception>
        /// <exception cref="TelefoneInvalidoException"></exception>
        /// <exception cref="SemConexaoComOBancoException"></exception>
        public Venda RegistrarVenda(VendaDTOSet venda)
        {
            Venda vendaConvertida = _mapper.Map<Venda>(venda);
            Venda vendaSalva = _servico.RegistrarVenda(vendaConvertida);
            return vendaSalva;
        }
    }
}
