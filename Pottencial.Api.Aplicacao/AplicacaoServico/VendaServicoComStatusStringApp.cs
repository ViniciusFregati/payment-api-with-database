﻿using EnumsNET;
using Pottencial.Api.Aplicacao.DTOs;
using Pottencial.Api.Aplicacao.Interfaces;
using Pottencial.Api.Dominio.Entidades;
using Pottencial.Api.Dominio.Excecoes;
using Pottencial.Api.Dominio.Models;

namespace Pottencial.Api.Aplicacao.AplicacaoServico
{
    /// <summary>
    /// Realiza a integração dos métodos existentes e dos DTOs, melhorando 
    /// a criação e visualização de seus objetos, inclusive, do status da venda.
    /// </summary>
    public class VendaServicoComStatusStringApp : IVendaServicoComStatusStringApp
    {
        private readonly IVendaServicoApp _servico;

        public VendaServicoComStatusStringApp(IVendaServicoApp servico)
        {
            _servico = servico;
        }

        /// <summary>
        /// Atualiza o status de uma venda.
        /// </summary>
        /// <param name="id">Id da venda que se deseja atualizar.</param>
        /// <param name="novoStatus">Status que se deseja atribuir a venda.</param>
        /// <returns>Retorna um booleano dizendo se deu certo a atualização</returns>
        /// <exception cref="AtualizacaoStatusInvalidoException"></exception>
        /// <exception cref="NaoEncontradoException"></exception>
        /// <exception cref="SemConexaoComOBancoException"></exception>
        public bool AtualizarStatusVenda(int id, EnumStatusVenda novoStatus)
        {
            return _servico.AtualizarStatusVenda(id, novoStatus);
        }

        /// <summary>
        /// Busca uma venda pelo seu Id.
        /// </summary>
        /// <param name="id">Id da venda que se deseja buscar.</param>
        /// <returns>Retorna o DTO da venda encontrada contendo a descrição no 
        /// Status Venda.</returns>
        /// <exception cref="NaoEncontradoException"></exception>
        /// <exception cref="SemConexaoComOBancoException"></exception>
        public VendaStatusStringDTOGet BuscarVendaPeloId(int id)
        {
            VendaDTOGet vendaRecebida = _servico.BuscarVendaPeloId(id);
            VendaStatusStringDTOGet vendaConvertida = new()
            {
                Data = vendaRecebida.Data,
                ItensVendidos = vendaRecebida.ItensVendidos,
                Vendedor = vendaRecebida.Vendedor,
                VendedorId = vendaRecebida.VendedorId,
                StatusVenda = vendaRecebida.StatusVenda.AsString(EnumFormat.Description)
            };

            return vendaConvertida;
        }

        /// <summary>
        /// Registra uma venda.
        /// </summary>
        /// <param name="venda">DTO da venda que se deseja registrar.</param>
        /// <returns>Retorna a venda registrada.</returns>
        /// <exception cref="ListaVaziaException"></exception>
        /// <exception cref="CpfInvalidoException"></exception>
        /// <exception cref="TelefoneInvalidoException"></exception>
        /// <exception cref="SemConexaoComOBancoException"></exception>
        public Venda RegistrarVenda(VendaDTOSet venda)
        {
            return _servico.RegistrarVenda(venda);
        }
    }
}
