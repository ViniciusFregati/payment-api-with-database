﻿namespace Pottencial.Api.Aplicacao.DTOs
{
    public class VendaStatusStringDTOGet
    {
        public string StatusVenda { get; set; }
        public int VendedorId { get; set; }
        public VendedorDTOGet? Vendedor { get; set; }
        public DateTime Data { get; set; }
        public List<ProdutoDTOGet>? ItensVendidos { get; set; }
    }
}
