﻿namespace Pottencial.Api.Aplicacao.DTOs
{
    /// <summary>
    /// Data transfer object de leitura de vendedor, ele possuí apenas as informações
    /// necessárias a serem exibidas como resposta de uma busca.
    /// </summary>
    public class VendedorDTOGet
    {
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
    }
}
