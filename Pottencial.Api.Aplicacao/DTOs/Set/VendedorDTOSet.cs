﻿using System.ComponentModel.DataAnnotations;

namespace Pottencial.Api.Aplicacao.DTOs
{
    /// <summary>
    /// Data transfer object de escrita do vendedor, ele possuí apenas as informações
    /// que precisam ser informadas na sua criação.
    /// </summary>
    public class VendedorDTOSet
    {
        [Required(ErrorMessage = "O campo nome é obrigatório")]
        public string? Nome { get; set; }
        [Required(ErrorMessage = "O campo cpf é obrigatório")]
        public string? Cpf { get; set; }
        [Required(ErrorMessage = "O campo telefone é obrigatório")]
        public string? Telefone { get; set; }
        [Required(ErrorMessage = "O campo e-mail é obrigatório")]
        public string? Email { get; set; }
    }
}
