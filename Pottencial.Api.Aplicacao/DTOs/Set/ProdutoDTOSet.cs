﻿using System.ComponentModel.DataAnnotations;

namespace Pottencial.Api.Aplicacao.DTOs
{
    public class ProdutoDTOSet
    {
        [Required(ErrorMessage = "O nome do produto é obrigatório")]
        public string? NomeDoProduto { get; set; }
    }
}
