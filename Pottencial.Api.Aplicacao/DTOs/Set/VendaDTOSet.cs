﻿using System.ComponentModel.DataAnnotations;

namespace Pottencial.Api.Aplicacao.DTOs
{
    /// <summary>
    /// Data transfer object de escrita da venda, ele possuí apenas as informações
    /// que precisam ser informadas na sua criação.
    /// </summary>
    public class VendaDTOSet
    {
        [Required(ErrorMessage = "O campo vendedor é obrigatório")]
        public VendedorDTOSet? Vendedor { get; set; }
        [Required(ErrorMessage = "A lista de itens vendidos é obrigatória")]
        public List<ProdutoDTOSet>? ItensVendidos { get; set; }
    }
}
