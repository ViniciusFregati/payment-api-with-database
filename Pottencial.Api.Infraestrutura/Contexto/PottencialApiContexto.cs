﻿using Microsoft.EntityFrameworkCore;
using Pottencial.Api.Dominio.Entidades;

namespace Pottencial.Api.Infraestrutura.Contexto
{
    /// <summary>
    /// Classe responsável por persistir os dados no banco de dados SQL Server e realizar seu mapeamento no mesmo.
    /// </summary>
    public class PottencialApiContexto : DbContext
    {
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Produto> Produtos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string stringconexao = "Server=localhost\\sqlexpress;Initial Catalog=PottencialBanco;Integrated Security=True; Encrypt=False";
            optionsBuilder.UseSqlServer(stringconexao);

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Venda>()
                .HasOne(venda => venda.Vendedor) // Venda possuí um vendedor
                .WithMany(vendedor => vendedor.Vendas) // Esse vendedor pode ter nenhuma ou várias vendas
                .HasForeignKey(venda => venda.VendedorId); // A venda tem a chave estrangêira que é o Id do vendedor.
        }
    }
}
