﻿using EnumsNET;
using Pottencial.Api.Dominio.Entidades;
using Pottencial.Api.Dominio.Excecoes;
using Pottencial.Api.Dominio.Interfaces.Repositorios;
using Pottencial.Api.Dominio.Models;
using Pottencial.Api.Infraestrutura.Contexto;
using Pottencial.Api.Infraestrutura.Interfaces;

namespace Pottencial.Api.Infraestrutura.Repositorio
{
    /// <summary>
    /// Classe responsável por realizar as interações necessárias com o banco de dados
    /// </summary>
    public class VendaDBRepositorio : IVendaRepositorio, IControlaBancoDeDados
    {
        private readonly ApiContexto _service = new();
        private readonly PottencialApiContexto _context = new();
        private readonly AtualizacaoDeStatusDeVenda atualizacaoStatus = new();

        /// <summary>
        /// Método utilizado para atualizar o status da venda, ele deve seguir as possíveis transições, caso contrário, lançará uma exceção.
        /// </summary>
        /// <param name="id">Id da venda que se deseja atualizar o status.</param>
        /// <param name="novoStatus">Novo status que deseja atribuir a venda.</param>
        /// <returns>Retorna um booleano dizendo se foi possível atualizar o status</returns>
        /// <exception cref="NaoEncontradoException"></exception>
        /// <exception cref="AtualizacaoStatusInvalidoException"></exception>
        /// <exception cref="SemConexaoComOBancoException"></exception>
        public bool AtualizarStatusVenda(int id, EnumStatusVenda novoStatus)
        {
            ConsegueSeConectarComBanco();
            Venda venda = BuscarVendaPeloId(id) ?? throw new NaoEncontradoException("A venda não foi encontrada");

            if (venda.StatusVenda == EnumStatusVenda.AguardandoPagamento
                && atualizacaoStatus.InicioAguardandoPagamento.Contains(novoStatus)
                || (venda.StatusVenda == EnumStatusVenda.PagamentoAprovado
                && atualizacaoStatus.InicioPagamentoAprovado.Contains(novoStatus))
                || (venda.StatusVenda == EnumStatusVenda.EnviadoParaTransportadora
                && atualizacaoStatus.InicioEnviadoParaTransportadora.Contains(novoStatus)))
            {
                venda.StatusVenda = novoStatus;
                _context.SaveChanges();
                return true;
            }

            string? descricaoStatusAtual = venda.StatusVenda.AsString(EnumFormat.Description);
            string? descricaoNovoStatus = novoStatus.AsString(EnumFormat.Description);
            throw new AtualizacaoStatusInvalidoException($"Não é possível alterar o status de {descricaoStatusAtual} para {descricaoNovoStatus}");
        }

        /// <summary>
        /// Método utilizado para buscar uma venda do banco de dados pelo seu Id, caso não encontre nenhuma,
        /// lança uma exceção.
        /// </summary>
        /// <param name="id">Id da venda que se deseja buscar</param>
        /// <returns>Retorna a venda encontrada</returns>
        /// <exception cref="NaoEncontradoException"></exception>
        /// <exception cref="SemConexaoComOBancoException"></exception>
        public Venda BuscarVendaPeloId(int id)
        {
            ConsegueSeConectarComBanco();
            Venda venda = _context.Vendas.FirstOrDefault(v => v.Id == id) ??
                throw new NaoEncontradoException("Venda não encontrada");

            Vendedor vendedor = _context.Vendedores.FirstOrDefault(v =>v.Id == venda.VendedorId);
            List<Produto> produtos = _context.Produtos.Where(p => p.VendaId == id).ToList();

            venda.Vendedor = vendedor;
            venda.ItensVendidos = produtos;

            return venda;
        }

        /// <summary>
        /// Registra uma nova venda no banco de dados, sendo avaliado a lista de compras, o cpf e telefone do vendedor.
        /// </summary>
        /// <param name="venda">Venda que se deseja registrar.</param>
        /// <returns>Retorna a venda adicionada.</returns>
        /// <exception cref="ListaVaziaException"></exception>
        /// <exception cref="CpfInvalidoException"></exception>
        /// <exception cref="TelefoneInvalidoException"></exception>
        /// <exception cref="SemConexaoComOBancoException"></exception>
        public Venda RegistrarVenda(Venda venda)
        {
            ConsegueSeConectarComBanco();
            _service.ValidaListaDeCompras(venda);
            _service.ValidaCpf(venda.Vendedor.Cpf);
            _service.ValidaTelefone(venda.Vendedor.Telefone);

            Vendedor vendedorEncontrado = _context.Vendedores.FirstOrDefault(v => v.Cpf == venda.Vendedor.Cpf);
            if (vendedorEncontrado == null || vendedorEncontrado.Cpf == "")
            {
                _context.Vendedores.Add(venda.Vendedor);
                venda.VendedorId = venda.Vendedor.Id;
            }
            else
            {
                venda.VendedorId = vendedorEncontrado.Id;
            }

            venda.Data = DateTime.Now.Date;
            venda.StatusVenda = EnumStatusVenda.AguardandoPagamento;
            foreach(Produto produto in venda.ItensVendidos)
            {
                produto.VendaId = venda.Id;
                _context.Produtos.Add(produto);
            }
            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return venda;
        }
        
        /// <summary>
        /// Inicia uma transação com o banco de dados.
        /// </summary>
        public void IniciaTransacao()
        {
            _context.Database.BeginTransaction();
        }

        /// <summary>
        /// Descartar uma transação do banco de dados.
        /// </summary>
        public void DescartaTransacao()
        {
            _context.ChangeTracker.Clear();
        }

        /// <summary>
        /// Verifica se consegue se conectar com o banco de dados
        /// </summary>
        /// <returns>Retorna true caso consiga com sucesso.</returns>
        /// <exception cref="SemConexaoComOBancoException"></exception>
        public bool ConsegueSeConectarComBanco()
        {
            bool conectou = _context.Database.CanConnect();

            if (!conectou)
                throw new SemConexaoComOBancoException("Não foi possível se conectar com o banco de dados");

            return true;
        }
    }
}
