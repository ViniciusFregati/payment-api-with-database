﻿using EnumsNET;
using Pottencial.Api.Dominio.Entidades;
using Pottencial.Api.Dominio.Excecoes;
using Pottencial.Api.Dominio.Interfaces.Repositorios;
using Pottencial.Api.Dominio.Models;
using Pottencial.Api.Infraestrutura.Contexto;

namespace Pottencial.Api.Infraestrutura.Repositorio
{
    /// <summary>
    /// Classe responsável por realizar as interações necessárias com nosso contexto
    /// </summary>
    public class VendaRepositorio : IVendaRepositorio
    {
        private readonly ApiContexto _contexto;
        private readonly AtualizacaoDeStatusDeVenda atualizacaoStatus = new();

        public VendaRepositorio()
        {
            _contexto = new ApiContexto();
        }

        /// <summary>
        /// Atualiza o status da venda.
        /// </summary>
        /// <param name="id">Id da venda que se deseja atualizar o status.</param>
        /// <param name="novoStatus">O novo status que se deseja atribuir a venda.</param>
        /// <returns>Retorna true se foi possível atualizar o status</returns>
        /// <exception cref="AtualizacaoStatusInvalidoException"></exception>
        /// <exception cref="NaoEncontradoException"></exception>
        public bool AtualizarStatusVenda(int id, EnumStatusVenda novoStatus)
        {
            Venda venda = BuscarVendaPeloId(id) ?? throw new NaoEncontradoException("A venda não foi encontrada");

            if (venda.StatusVenda == EnumStatusVenda.AguardandoPagamento
                && atualizacaoStatus.InicioAguardandoPagamento.Contains(novoStatus))
            {
                venda.StatusVenda = novoStatus;
                return true;
            }
            else if (venda.StatusVenda == EnumStatusVenda.PagamentoAprovado
                && atualizacaoStatus.InicioPagamentoAprovado.Contains(novoStatus))
            {
                venda.StatusVenda = novoStatus;
                return true;
            }
            else if (venda.StatusVenda == EnumStatusVenda.EnviadoParaTransportadora
                && atualizacaoStatus.InicioEnviadoParaTransportadora.Contains(novoStatus))
            {
                venda.StatusVenda = novoStatus;
                return true;
            }

            string? descricaoStatusAtual = venda.StatusVenda.AsString(EnumFormat.Description);
            string? descricaoNovoStatus = novoStatus.AsString(EnumFormat.Description);
            throw new AtualizacaoStatusInvalidoException($"Não é possível alterar o status de {descricaoStatusAtual} para {descricaoNovoStatus}");
        }

        /// <summary>
        /// Busca uma venda do contexto.
        /// </summary>
        /// <param name="id">Id da venda que se deseja recuperar.</param>
        /// <returns>Retorna a venda encontrada.</returns>
        /// <exception cref="NaoEncontradoException"></exception>
        public Venda BuscarVendaPeloId(int id)
        {
            Venda vendaBuscada = _contexto.Vendas.FirstOrDefault(v => v.Id == id) ??
                throw new NaoEncontradoException("Venda não encontrada");

            return vendaBuscada;
        }

        /// <summary>
        /// Registra uma nova venda no contexto, caso dê erro, lança exceção.
        /// </summary>
        /// <param name="venda">Venda que se deseja registrar.</param>
        /// <returns>Retorna true caso consiga realizar o cadastro, caso não, lançará exceções.</returns>
        /// <exception cref="ListaVaziaException"></exception>
        /// <exception cref="CpfInvalidoException"></exception>
        /// <exception cref="TelefoneInvalidoException"></exception>
        public Venda RegistrarVenda(Venda venda)
        {
            Venda vendaSalva = _contexto.SalvarVenda(venda);
            return vendaSalva;
        }
    }
}
