﻿namespace Pottencial.Api.Infraestrutura.Interfaces
{
    public interface IControlaBancoDeDados
    {
        public void IniciaTransacao();
        public void DescartaTransacao();
        public bool ConsegueSeConectarComBanco();
    }
}
