﻿using Pottencial.Api.Dominio.Entidades;

namespace Pottencial.Api.Infraestrutura.Contexto
{
    public interface IContext
    {
        public Vendedor SalvarVendedor(Vendedor vendedor);
        public Venda SalvarVenda(Venda venda);
    }
}
