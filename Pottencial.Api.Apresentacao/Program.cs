using System.Reflection;
using System.Text.Json.Serialization;
using Pottencial.Api.Aplicacao.AplicacaoServico;
using Pottencial.Api.Aplicacao.Interfaces;
using Pottencial.Api.Apresentacao.Controllers;
using Pottencial.Api.Dominio.Interfaces.Repositorios;
using Pottencial.Api.Dominio.Interfaces.Servicos;
using Pottencial.Api.Dominio.Servicos;
using Pottencial.Api.Infraestrutura.Contexto;
using Pottencial.Api.Infraestrutura.Repositorio;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<PottencialApiContexto>();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddControllers().AddJsonOptions(opcao =>
    {
        opcao.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
    }
);

builder.Services.AddSingleton<IVendaRepositorio, VendaDBRepositorio>();
builder.Services.AddSingleton<IVendaServico, VendaServico>();
builder.Services.AddSingleton<IVendaServicoApp, VendaServicoApp>();
builder.Services.AddSingleton<IVendaServicoComStatusStringApp, VendaServicoComStatusStringApp>();
builder.Services.AddSingleton<VendaController, VendaController>();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    // Permitiu adicionar comentários na documentação do swagger
    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
